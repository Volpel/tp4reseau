﻿#! usr/bin/env perl

use MIME::Lite;
use IO::Socket;
use Digest::MD5 qw(md5_hex);

my $running = 0;

print "Bienvenue dans votre messagerie!\r\n\r\n";

print "=== AUTHENTIFICATION ===\r\n";

print "Utilisateur: ";
my $user = <STDIN>;
chomp($user);

print "Mot de passe: ";
my $pw = <STDIN>;
chomp($pw);

my $hashedPassword = md5_hex($pw);

print "Username: $user\r\n";
print "Hash: $hashedPassword\r\n";

# ============================================
# ==== Authentification auprès du serveur ====
# ============================================ 

$server = IO::Socket::INET->new(
		PeerHost => "localhost",
		PeerPort => 25,
		Proto => "tcp"
)
or die "Connexion vers localhost par le port 25 impossible.\r\m";

print $server "$user\r\n";
print $server "$hashedPassword\r\n";

my $authentication;
$server->recv($authentication, 2048);

if($authentication eq "authenticated") {
	print "Vous etes maintenant connecte en tant que $user\@reseauglo.ca.\r\n";
	$running = 1;
}
else {
	print "Le nom d'utilisateur ou le mot de passe est invalide.\r\n";
}	

# ===========================================
# =========== Programme principal ===========
# =========================================== 

my $selection = 0;
while($running == 1) {

	$selection = 0;
	
	# Affichage du menu
	print "\r\n=== Menu ===\r\n";
	print "1. Envoi de courriels\r\n";
	print "2. Consultation de courriels\r\n";
	print "3. Statistiques\r\n";
	print "4. Mode administrateur\r\n";
	print "5. Quitter\r\n";
	
	print "Selection: ";
	$selection = <STDIN>;
	chomp($selection);
	
	if($selection == 1) {
		if($user eq "admin") {
			print "Erreur: Vous ne pouvez pas envoyer de courriels en tant qu'administrateur.\r\n";
		}
		else {
		
			# On notifie le serveur qu'on veut envoyer un message
			print $server "NewMessage\r\n";
			
			# On construit le message et on envoie les champs au serveur
			print "\r\nAdresse de destination: ";
			my $dest = <STDIN>;
			print $server "$dest";
			
			print "Adresse en copie conforme: ";
			my $cc = <STDIN>;
			print $server "$cc";
			
			print "Sujet: ";
			my $subject = <STDIN>;
			print $server "$subject";
			
			print "Corps du message: ";
			my $body = <STDIN>;
			print $server "$body";
			
			# Confirmation de la part du serveur
			my $answer = <$server>;
			if($answer eq "sent\r\n") {
				print "Votre message a ete envoye!\r\n";
			}
			else {
				print "Le message n'a pas pu etre envoye.\r\n";
			}
			
		}
	}
	elsif($selection == 2) {
		
		# On notifie le serveur qu'on veut consulter les messages
		print $server "ReadMessages\r\n";
		
		my $subjectSent = undef;
		while($subjectSent = <$server>) {
			if($subjectSent ne "endoffile\r\n") {
				print("$subjectSent");
			}
			else {
				last;
			}
		}
		print "Selection: ";
		my $msgToRead = <STDIN>;
		print $server "$msgToRead";
		
		print "\r\n=== MESSAGE ===\r\n";
		my $from = <$server>;
		my $cc = <$server>;
		my $subject = <$server>;
		my $body = "";
		while(my $line = <$server>) {
			if($line ne "endoffile\r\n") {
				$body = $body.$line;
			}
			else {
				last;
			}
		}
		
		print "$from";
		print "$cc";
		print "$subject";
		print "$body\r\n";
		
		print "Appuyez sur entrée pour revenir au menu...";
		my $wait = <STDIN>;
	}
	elsif($selection == 3) {
	
		# On notifie le serveur qu'on veut consulter les statistiques
		print $server "Stats\r\n";
		
		my $subjectSent = undef;
		while($subjectSent = <$server>) {
			if ($subjectSent ne "endoffile\r\n") {
				print("$subjectSent");
			}
			else {
				last;
			}
		}
		
		my $nbMsg = <$server>;
		print "Nombre de messages: $nbMsg";
		
		my $dirSize = <$server>;
		$dirSize =~ s/\r|\n//g;
		print "Taille du repertoire: $dirSize octets\r\n"; # Ne compte pas config.txt
		
		print "\r\nAppuyez sur entrée pour revenir au menu...";
		my $wait = <STDIN>;
	}
	elsif($selection == 4) {
	
		if($user ne "admin") {
			print "Erreur: Vous n'etes pas connecte en tant qu'administrateur.\r\n";
		}
		else {
		
			# On notifie le serveur qu'on veut entrer dans le mode administrateur
			print $server "AdminMode\r\n";
			
			print "Bienvenue dans le mode administrateur!\r\n";
			
			my $userSent = undef;
			while($userSent = <$server>) {
				if ($userSent ne "endofusers\r\n") {
					print("$userSent");
				}
				else {
					last;
				}
			}
			
			print "Selection: ";
			my $userSelection = <STDIN>;
			print $server "$userSelection";
			
			my $subjectSent = undef;
			while($subjectSent = <$server>) {
				if ($subjectSent ne "endoffile\r\n") {
					print("$subjectSent");
				}
				else {
					last;
				}
			}
		
			my $nbMsg = <$server>;
			print "Nombre de messages: $nbMsg";
		
			my $dirSize = <$server>;
			$dirSize =~ s/\r|\n//g;
			print "Taille du repertoire: $dirSize octets\r\n"; # Ne compte pas config.txt
			
			#Entrer un numéro pour accéder aux statistiques de la personne.
			print "\r\nAppuyez sur une touche pour revenir au menu...";
			my $wait = <STDIN>;
		}
		
	}
	elsif($selection == 5) {
		
		print $server "Quit\r\n";
		$running = 0;
	}
	
}

print "\r\nExecution terminee...";
close($server);