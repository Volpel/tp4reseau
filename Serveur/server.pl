#! usr/bin/env perl

use IO::Socket;
use MIME::Lite;
use Getopt::Long;
use File::stat;

$server = IO::Socket::INET->new(Proto => "tcp",
				LocalPort => 25,
				Listen => SOMAXCONN,
				Reuse => 1)
or die "Impossible de se connecter sur le port 25 en localhost";

print "En train d'ecouter sur le port 25...\r\n";

while(my $client = $server->accept()) 
{
	$client->autoflush(1);

	# ======================
	# == AUTHENTIFICATION ==
	# ======================
	
	my $user = <$client>;
	$user =~ s/\r|\n//g; # CHOMP MULTIPLATEFORME
	print "Username: $user\r\n";
	
	my $pw = <$client>;
	$pw =~ s/\r|\n//g;
	print "Hashed password: $pw\r\n";
	
	print "\r\nSearching for $user directory...\r\n";
	if(-d "$user") {
	
		print "Utilisateur trouve. Verification du mot de passe en cours...\r\n";
		open(my $pwFile, "<", "$user/config.txt") or die "Erreur: Impossible d'ouvrir le fichier config.txt...\r\n";
		
		my $realPw = <$pwFile>;
		if ($pw eq $realPw) {
			print "Authentifie. Envoi de la reponse au client courant...\r\n";
			print $client "authenticated";
			$connected = 1;
		}
		else {
			print "Erreur: Authentification impossible.\r\n";
		}
		
		close $pwFile;
	}
	
	# =========================
	# == PROGRAMME PRINCIPAL ==
	# =========================
	
	while($connected == 1) {
	
		my $mode = <$client>;
		$mode =~ s/\r|\n//g; 
		
		if($mode eq "NewMessage") {
		
			$to = <$client>;
			$to =~ s/\r|\n//g;
			$cc = <$client>;
			$cc =~ s/\r|\n//g;
			$sbj = <$client>;
			$sbj =~ s/\r|\n//g;
			$body = <$client>; 
			$body =~ s/\r|\n//g;
			
			
			
			if($to =~ /(([^@]+)@(.+))/) {
				if($3 eq "reseauglo.ca") {
				
					# Création du fichier contenant le message dans la boîte de réception du destinataire
					my $fileTarget = "";
					if(opendir my $userDir, "$2") {
						closedir $userDir;
						$fileTarget = $2."/".$sbj.".txt";
					}
					else {
						$fileTarget = "DESTERREUR/".$sbj.".txt";
					}
					
					open(my $msgFile, ">".$fileTarget) or die "Cannot create $fileTarget";
					
					print $msgFile "Source: $user\@reseauglo.ca\r\n";
					print $msgFile "Cc: $cc\r\n";
					print $msgFile "Sujet: $sbj\r\n\r\n";
					print $msgFile "$body";
					
					close $msgFile;
					
					print $client "sent\r\n";
				}
				else {
				print "Adresse distante detectee : $to \r\n";
				
					my $msg = MIME::Lite->new(
							From =>  $user.'\@reseauglo.ca',
							To => $to,
							Cc => $cc,
							Subject => $sbj,
							Data => $body
					);					
					$msg->send('smtp', "smtp.ulaval.ca", Timeout=>60);
					
					# On notifie le client que le message a bien été envoyé
					print $client "sent";
				}
			}
			
		}
		elsif($mode eq "ReadMessages") {
			
			print("Read Messages Mode called by client\r\n");
			
			opendir my $mailbox, "$user" or die "Erreur: Impossible d'ouvrir le dossier du client.";
			
			#On liste tous les messages du client
			my $index = 1;
			my @fileIndex = undef;
			while(my $currentMsg = readdir $mailbox) {
				if($currentMsg ne "config.txt" && $currentMsg ne "." && $currentMsg ne "..") {
					$currentMsg =~ s/\..*+$//;
					print $client "$index. $currentMsg\r\n";
					push @fileIndex, "$currentMsg.txt";
					$index++;
				}
				
			}
			print $client "endoffile\r\n";
			closedir $mailbox;
			
			# On requiert le numéro du message du client
			my $msgToRead = <$client>;
			$msgToRead =~ s/\r|\n//g;
			print "Recieved client selection: $msgToRead\r\n";
			
			open(my $msgFile, "<", "$user/".@fileIndex[$msgToRead]) or die "Erreur: Impossible d'ouvrir le fichier.";
			
			# ... Lire le message et envoyer au client
			my $from = <$msgFile>;
			my $cc = <$msgFile>;
			my $sbj = <$msgFile>;
			
			print $client "$from";
			print $client "$cc";
			print $client "$sbj";
			while (my $line = <$msgFile>) {
				print "Sending $line to client...\r\n";
				print $client "$line\r\n";
			}
			print "Sending EOF to client\r\n";
			print $client "endoffile\r\n";
			
			close $msgFile;
		
		}
		elsif($mode eq "Stats") {
		
			opendir my $mailbox, "$user" or die "Erreur: Impossible d'ouvrir le dossier du client.";

			my $nbEmails = 1;
			my $mailboxSize = 0;
			while(readdir $mailbox) {
				if($_ ne "config.txt" && $_ ne "." && $_ ne "..")
				{
					$mailboxSize += -s $user."/".$_;
					$_ =~ s/\..*+$//; # On enlève le .txt...
					print $client "$nbEmails. $_\r\n";
					$nbEmails++;
				}
			}
			print $client "endoffile\r\n";
			
			$nbEmails -= 1;
			print $client "$nbEmails\r\n";
			print $client "$mailboxSize\r\n";
		
		}
		elsif($mode eq "AdminMode") {
			
			use Cwd;
			
			my $root = getcwd;
			opendir my $users, $root;
			
			my $userIndex = 1;
			my @listOfUsers = undef;
			while(my $currentUser = readdir $users) {
				next unless (-d "$currentUser");
				
				if($currentUser ne "DESTERREUR" && $currentUser ne "." && $currentUser ne "..") {
					push @listOfUsers, "$currentUser";
					print $client "$userIndex. $currentUser\r\n";
					$userIndex++;
				}
				
			}
			print $client "endofusers\r\n";
			
			my $chosenUserSelection = <$client>;
			$chosenUserSelection =~ s/\r|\n//g;
			my $chosenUser = @listOfUsers[$chosenUserSelection];
			
			opendir my $mailbox, "$chosenUser" or die "Erreur: Impossible d'ouvrir la boîte de reception selectionnee.";

			my $nbEmails = 1;
			my $mailboxSize = 0;
			while(readdir $mailbox) {
				if($_ ne "config.txt" && $_ ne "." && $_ ne "..")
				{
					$mailboxSize += -s $chosenUser."/".$_;
					$_ =~ s/\..*+$//; # On enlève le .txt...
					print $client "$nbEmails. $_\r\n";
					$nbEmails++;
				}
			}
			print $client "endoffile\r\n";
			
			$nbEmails -= 1;
			print $client "$nbEmails\r\n";
			print $client "$mailboxSize\r\n";
			
		}
		elsif($mode eq "Quit") {
			print "Mode: Quit\r\n";
			$connected = 0;
		}
		
	}
	
	close($client);
}
